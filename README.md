# chart_viewer


# production 
```
export COMPOSE=docker-compose-pro.yml
docker-compose build
docker-compose run client npm install
docker-compose run client npm run build
docker-compose run client npm run start # work as nuxt start

curl http://localhost:3000/
# to check
```


# local
```
export COMPOSE_FILE=docker-compose-dev.yml

docker-compose build
docker-compose up -d
# access to http://localhost:3000/
```

# access to api
`docker-compose run api /bin/bash`

# upgrade pipenv
update pipenv
